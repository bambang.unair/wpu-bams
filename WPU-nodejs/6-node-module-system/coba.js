// console.log("hello world");
function cetakNama(nama) {
  return `hi nama saya ${nama}`;
}

const PI = 3.14;
const mahasiswa = {
  nama: "Raihan",
  umur: 20,
  cetakMhs() {
    return `halo nama saya ${this.nama} dan saya ${this.umur}tahun.`;
  },
};

// module.exports.cetakNama = cetakNama;
// module.exports.PI = PI;
// module.exports.mahasiswa = mahasiswa;

module.exports = {
  cetakNama,
  PI,
  mahasiswa,
};
