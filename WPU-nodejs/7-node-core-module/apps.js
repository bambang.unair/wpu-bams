//core modules
//file system
const fs = require("fs");
const { readFile } = require("fs/promises");

//menuliskan string ke file synchronous
// fs.writeFileSync("test.txt", "hello world syn");

// //menuliskan string ke file asynchronous
// fs.writeFile("test.txt", " hello world asynchronous", (e) => {
//   console.log(e);
// });

//membaca file synchronous
// const data = fs.readFileSync("test.txt", "utf-8");
// console.log(data);

//membaca file asynchronous
// fs.readFile("test.txt", "utf-8", (err, data) => {
//   if (err) throw Err;
//   console.log(data);
// });

//Read Line
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});
rl.question("masukkan nama anda : ", (nama) => {
  console.log(`terimakasih ${nama}`);
  rl.close();
});
